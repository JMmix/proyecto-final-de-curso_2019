# Proyecto-final-de-curso_2019
Repositorio con todos los archivos que se han usado en el proyecto.  
Dentro de este repositorio hay tres directorios:  
* helm: Dentro de este directorio esta la configuración para la service account que usará helm
* helm-chart: 
* vagrant: Dentro de este directorio tenemos la Vagrantfile que se ha usado para crear la MV  

En el raíz del repositorio tenemos la Jenkinsfile que es donde se encuentra todo el código del pipeline y la Dockerfile que se usa para crear la imagen Docker  
Para usar la máquina virtual que ha sido entregado con el proyecto se require primero del software Vagrant y Virtualbox instalados para recuperar la máquina virtual.
Para recuperarla primero hay que copiar el directorio centos-kubenetes del pendrive entregado al host.
Después hay que abrir una consola en ese directorio, y ejecutamos el comando `vagrant box add --name centos-kubenetes`.  
Contraseña Jenkins: r9iZrHU0Yu  
`minikube start --cpus 4 --memory 6144 --vm-driver=none --extra-config=kubelet.cgroup-driver=systemd`