FROM nginx:1.17-alpine
RUN rm -rf /usr/share/nginx/html/*
ADD http://192.168.33.10:31975/repository/npm-private/angular-jump-start/-/angular-jump-start-0.0.0.tgz /usr/share/nginx/html
RUN cd /usr/share/nginx/html && \
tar -xvf angular-jump-start-0.0.0.tgz && \\
mv package/dist/* . && \\
rm -rf package